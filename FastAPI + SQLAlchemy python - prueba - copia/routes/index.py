
from fastapi import APIRouter
from controllers.controlador_prueba import primer_servicio,servicio_prueba_usuarios,servicio_prueba_usuarios_2,servicio_prueba_usuarios_3

router = APIRouter()

@router.get('/primer_ruta')
async def middleware_primer_controlador():
    
    respuesta = primer_servicio()
    return respuesta


@router.get('/segunda_ruta')
async def middleware_segundo_controlador():

    respuesta = servicio_prueba_usuarios()

    return respuesta

@router.get('/tercer_ruta')
async def middleware_tercer_controlador():
    
    respuesta = servicio_prueba_usuarios_2()

    return respuesta

@router.get('/cuarta_ruta')
async def middleware_tercer_controlador():
    
    respuesta = servicio_prueba_usuarios_3()

    return respuesta