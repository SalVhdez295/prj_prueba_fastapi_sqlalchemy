from fastapi import FastAPI,Request
from routes.index import router
from starlette.responses import JSONResponse, Response
from services.authMiddleware import autorizacion_middleware
app = FastAPI()
# doc de middleware https://issuecloser.com/blog/how-to-create-middlewares-with-fastapi
@app.middleware("http")
async def autorizacion_usuario(request:Request, call_next): #las cabeceras son obligatorias.
    valor = True

    respuesta = autorizacion_middleware(valor)
 
    if respuesta == True:
        response = await call_next(request)
        return response
    else:
        return JSONResponse(content={"message":"no permitido"},status_code= 401)


# @app es xq se refiere al app de arriba que es instancia de FastAPI.
#@app.get('/')
#def helloworld():
#    return "Hola Mundo"

#en vez de eso, usaremos un archivo encargado de rutas.
app.include_router(router)

