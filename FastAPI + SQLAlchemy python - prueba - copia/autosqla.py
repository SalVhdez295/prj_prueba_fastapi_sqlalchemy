# coding: utf-8
from sqlalchemy import Boolean, Column, Date, DateTime, ForeignKey, Integer, Numeric, String, Time, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class TblNTipoUsuario(Base):
    __tablename__ = 'tbl_n_tipo_usuario'
    __table_args__ = {'schema': 'sch_seguridad'}

    id_tipo_usuario = Column(Integer, primary_key=True, server_default=text("nextval('sch_seguridad.tbl_n_tipo_usuario_id_tipo_usuario_seq'::regclass)"))
    tipo_usuario = Column(String(255), nullable=False)
    is_active = Column(Boolean, nullable=False)
    fecha_creacion = Column(DateTime, nullable=False)
    fecha_modificacion = Column(DateTime, nullable=False)


class TblNBanco(Base):
    __tablename__ = 'tbl_n_banco'

    id_banco = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_banco_id_banco_seq'::regclass)"))
    nombre_banco = Column(String(60), nullable=False)


class TblNCasaComercial(Base):
    __tablename__ = 'tbl_n_casa_comercial'

    id_casa_comercial = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_casa_comercial_id_casa_comercial_seq'::regclass)"))
    casa_comercial = Column(String(30), nullable=False)


class TblNCuentaBancaria(Base):
    __tablename__ = 'tbl_n_cuenta_bancaria'

    id_cuenta_bancaria = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_cuenta_bancaria_id_cuenta_bancaria_seq'::regclass)"))
    nombre_cuenta_bancaria = Column(String(255), nullable=False)
    numero_cuenta_bancaria = Column(String(255), nullable=False)
    activo = Column(Boolean, nullable=False)


class TblNEstadoAsignacionPedido(Base):
    __tablename__ = 'tbl_n_estado_asignacion_pedido'

    id_estado_asignacion_pedido = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_asignacion_pedido_id_estado_asignacion_pedido_seq'::regclass)"))
    estado_asignacion_pedido = Column(String(30), nullable=False)


class TblNEstadoDetalleAsignacionPedido(Base):
    __tablename__ = 'tbl_n_estado_detalle_asignacion_pedido'

    id_estado_detalle_asignacion_pedido = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_detalle_asignaci_id_estado_detalle_asignacion__seq'::regclass)"))
    estado_detalle_asignacion_pedido = Column(String(30), nullable=False)


class TblNEstadoDetalleConsumo(Base):
    __tablename__ = 'tbl_n_estado_detalle_consumo'

    id_estado_detalle_consumo = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_detalle_consumo_id_estado_detalle_consumo_seq'::regclass)"))
    estado_detalle_consumo = Column(String(30), nullable=False)


class TblNEstadoDetalleDevolucion(Base):
    __tablename__ = 'tbl_n_estado_detalle_devolucion'

    id_estado_detalle_devolucion = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_detalle_devolucio_id_estado_detalle_devolucion_seq'::regclass)"))
    estado_detalle_devolucion = Column(String(30))


class TblNEstadoDetallePedido(Base):
    __tablename__ = 'tbl_n_estado_detalle_pedido'

    id_estado_detalle_pedido = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_detalle_pedido_id_estado_detalle_pedido_seq'::regclass)"))
    estado_detalle_pedido = Column(String(60), nullable=False)


class TblNEstadoDocumentoConsumo(Base):
    __tablename__ = 'tbl_n_estado_documento_consumo'

    id_estado_documento_consumo = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_documento_consumo_id_estado_documento_consumo_seq'::regclass)"))
    estado_documento_consumo = Column(String(20), nullable=False)
    fecha_creacion = Column(Date, nullable=False)
    fecha_modificacion = Column(Date, nullable=False)
    activo = Column(Boolean, nullable=False)


class TblNEstadoLote(Base):
    __tablename__ = 'tbl_n_estado_lote'

    id_estado_lote = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_lote_id_estado_lote_seq'::regclass)"))
    estado_lote = Column(String(40), nullable=False)


class TblNEstadoNegociacion(Base):
    __tablename__ = 'tbl_n_estado_negociacion'

    id_estado_negociacion = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_negociacion_id_estado_negociacion_seq'::regclass)"))
    estado_negociacion = Column(String(20), nullable=False)


class TblNEstadoOfrecimiento(Base):
    __tablename__ = 'tbl_n_estado_ofrecimiento'

    id_estado_ofrecimiento = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_ofrecimiento_id_estado_ofrecimiento_seq'::regclass)"))
    estado_ofrecimiento = Column(String(20), nullable=False)


class TblNEstadoPedido(Base):
    __tablename__ = 'tbl_n_estado_pedido'

    id_estado_pedido = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_pedido_id_estado_pedido_seq'::regclass)"))
    estado_pedido = Column(String(50), nullable=False)


class TblNEstadoProducto(Base):
    __tablename__ = 'tbl_n_estado_producto'

    id_estado_producto = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_producto_id_estado_producto_seq'::regclass)"))
    estado_producto = Column(String(30), nullable=False)


class TblNEstadoSolicitudDevolucion(Base):
    __tablename__ = 'tbl_n_estado_solicitud_devolucion'

    id_estado_solicitud_devolucion = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_estado_solicitud_devolu_id_estado_solicitud_devolucio_seq'::regclass)"))
    estado_solicitud_devolucion = Column(String(20), nullable=False)


class TblNFormaPago(Base):
    __tablename__ = 'tbl_n_forma_pago'

    id_forma_pago = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_forma_pago_id_forma_pago_seq'::regclass)"))


class TblNPo(Base):
    __tablename__ = 'tbl_n_pos'

    id_pos = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_pos_id_pos_seq'::regclass)"))
    pos = Column(String(30))


class TblNProveedor(Base):
    __tablename__ = 'tbl_n_proveedor'

    id_proveedor = Column(Integer, primary_key=True)
    nombre_proveedor = Column(String(30), nullable=False)
    activo = Column(Boolean, nullable=False)


class TblNRepresentanteMarca(Base):
    __tablename__ = 'tbl_n_representante_marca'

    id_representante_marca = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_representante_marca_id_representante_marca_seq'::regclass)"))
    representante_marca = Column(String(30), nullable=False)
    activo = Column(Boolean, nullable=False)


class TblNTipoBodega(Base):
    __tablename__ = 'tbl_n_tipo_bodega'

    id_tipo_bodega = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_tipo_bodega_id_tipo_bodega_seq'::regclass)"))
    tipo_bodega = Column(String(30), nullable=False)


class TblNTipoInventario(Base):
    __tablename__ = 'tbl_n_tipo_inventario'

    id_tipo_inventario = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_tipo_inventario_id_tipo_inventario_seq'::regclass)"))
    tipo_inventario = Column(String(30), nullable=False)


class TblNTipoNegociacion(Base):
    __tablename__ = 'tbl_n_tipo_negociacion'

    id_tipo_negociacion = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_tipo_negociacion_id_tipo_negociacion_seq'::regclass)"))
    tipo_negociacion = Column(String(60), nullable=False)


class TblNTipoPago(Base):
    __tablename__ = 'tbl_n_tipo_pago'

    id_tipo_pago = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_tipo_pago_id_tipo_pago_seq'::regclass)"))
    tipo_pago = Column(String(20), nullable=False)


class TblNTipoSucursal(Base):
    __tablename__ = 'tbl_n_tipo_sucursal'

    id_tipo_sucursal = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_tipo_sucursal_id_tipo_sucursal_seq'::regclass)"))
    tipo_sucursal = Column(String(30), nullable=False)


class TblNUsuario(Base):
    __tablename__ = 'tbl_n_usuario'
    __table_args__ = {'schema': 'sch_seguridad'}

    id_usuario = Column(Integer, primary_key=True, server_default=text("nextval('sch_seguridad.tbl_n_usuario_id_usuario_seq'::regclass)"))
    id_f_empleado = Column(Integer)
    nombre_usuario = Column(String(40))
    contrasena_usuario = Column(String(255))
    activo = Column(Boolean)
    is_user_admin = Column(Boolean)
    id_f_tipo_usuario = Column(ForeignKey('sch_seguridad.tbl_n_tipo_usuario.id_tipo_usuario'))

    tbl_n_tipo_usuario = relationship('TblNTipoUsuario')


class TblNAlianza(Base):
    __tablename__ = 'tbl_n_alianza'

    id_alianza = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_alianza_id_alianza_seq'::regclass)"))
    id_f_casa_comercial = Column(ForeignKey('tbl_n_casa_comercial.id_casa_comercial'), nullable=False)
    id_f_representante_marca = Column(ForeignKey('tbl_n_representante_marca.id_representante_marca'), nullable=False)
    cobra_iva = Column(Boolean, nullable=False)
    cobra_iva_retenido = Column(Boolean, nullable=False)
    activo = Column(Boolean)
    fecha_creacion = Column(Date, nullable=False)
    fecha_modificacion = Column(Date, nullable=False)

    tbl_n_casa_comercial = relationship('TblNCasaComercial')
    tbl_n_representante_marca = relationship('TblNRepresentanteMarca')


class TblNAsignacionPedido(Base):
    __tablename__ = 'tbl_n_asignacion_pedido'

    id_asignacion_pedido = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_asignacion_pedido_id_asignacion_pedido_seq'::regclass)"))
    id_f_estado_asignacion_pedido = Column(ForeignKey('tbl_n_estado_asignacion_pedido.id_estado_asignacion_pedido'), nullable=False)
    fecha_creacion = Column(Date)
    fecha_modificacion = Column(Date)
    cantidad_escogida = Column(Integer)

    tbl_n_estado_asignacion_pedido = relationship('TblNEstadoAsignacionPedido')


class TblNBodega(Base):
    __tablename__ = 'tbl_n_bodega'

    id_bodega = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_bodega_id_bodega_seq'::regclass)"))
    nombre_bodega = Column(String(30), nullable=False)
    fecha_creacion = Column(Date, nullable=False)
    fecha_modificacion = Column(Date, nullable=False)
    activo = Column(Boolean, nullable=False)
    id_f_tipo_bodega = Column(ForeignKey('tbl_n_tipo_bodega.id_tipo_bodega'), nullable=False)

    tbl_n_tipo_bodega = relationship('TblNTipoBodega')


class TblNEntidadBancaria(Base):
    __tablename__ = 'tbl_n_entidad_bancaria'

    id_entidad_bancaria = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_entidad_bancaria_id_entidad_bancaria_seq'::regclass)"))
    id_f_banco = Column(ForeignKey('tbl_n_banco.id_banco'), nullable=False)
    id_f_pos = Column(ForeignKey('tbl_n_pos.id_pos'))
    id_f_cuenta_bancaria = Column(ForeignKey('tbl_n_cuenta_bancaria.id_cuenta_bancaria'))
    activo = Column(Boolean, nullable=False)

    tbl_n_banco = relationship('TblNBanco')
    tbl_n_cuenta_bancaria = relationship('TblNCuentaBancaria')
    tbl_n_po = relationship('TblNPo')


class TblNOfrecimiento(Base):
    __tablename__ = 'tbl_n_ofrecimiento'

    id_ofrecimiento = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_ofrecimiento_id_ofrecimiento_seq'::regclass)"))
    id_f_estado_ofrecimiento = Column(ForeignKey('tbl_n_estado_ofrecimiento.id_estado_ofrecimiento'), nullable=False)
    fecha_creacion = Column(Date)

    tbl_n_estado_ofrecimiento = relationship('TblNEstadoOfrecimiento')


class TblNPedido(Base):
    __tablename__ = 'tbl_n_pedido'

    id_pedido = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_pedido_id_pedido_seq'::regclass)"))
    id_f_estado_pedido = Column(ForeignKey('tbl_n_estado_pedido.id_estado_pedido'), nullable=False)

    tbl_n_estado_pedido = relationship('TblNEstadoPedido')


class TblNProducto(Base):
    __tablename__ = 'tbl_n_producto'

    id_producto = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_producto_id_producto_seq'::regclass)"))
    nombre_producto = Column(String(60), nullable=False)
    fotografia = Column(String(30))
    id_f_estado_producto = Column(ForeignKey('tbl_n_estado_producto.id_estado_producto'), nullable=False)
    fecha_creacion = Column(Date, nullable=False)
    fecha_modificacion = Column(Date, nullable=False)

    tbl_n_estado_producto = relationship('TblNEstadoProducto')


class TblNRepresentanteProveedor(Base):
    __tablename__ = 'tbl_n_representante_proveedor'

    id_representante_proveedor = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_representante_proveedor_id_representante_proveedor_seq'::regclass)"))
    id_f_representante_marca = Column(ForeignKey('tbl_n_representante_marca.id_representante_marca'), nullable=False)
    id_f_proveedor = Column(ForeignKey('tbl_n_proveedor.id_proveedor'), nullable=False)

    tbl_n_proveedor = relationship('TblNProveedor')
    tbl_n_representante_marca = relationship('TblNRepresentanteMarca')


class TblNSolicitudDevolucion(Base):
    __tablename__ = 'tbl_n_solicitud_devolucion'

    id_solicitud_devolucion = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_solicitud_devolucion_id_solicitud_devolucion_seq'::regclass)"))
    id_f_estado_solicitud_devolucion = Column(ForeignKey('tbl_n_estado_solicitud_devolucion.id_estado_solicitud_devolucion'), nullable=False)
    fecha_creacion = Column(Date)
    fecha_modificacion = Column(Date)

    tbl_n_estado_solicitud_devolucion = relationship('TblNEstadoSolicitudDevolucion')


class TblNSucursal(Base):
    __tablename__ = 'tbl_n_sucursal'

    id_sucursal = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_sucursal_id_sucursal_seq'::regclass)"))
    id_f_tipo_sucursal = Column(ForeignKey('tbl_n_tipo_sucursal.id_tipo_sucursal'), nullable=False)

    tbl_n_tipo_sucursal = relationship('TblNTipoSucursal')


class TblNDetallePedido(Base):
    __tablename__ = 'tbl_n_detalle_pedido'

    id_detalle_pedido = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_detalle_pedido_id_detalle_pedido_seq'::regclass)"))
    id_f_estado_detalle_pedido = Column(ForeignKey('tbl_n_estado_detalle_pedido.id_estado_detalle_pedido'), nullable=False)
    id_f_pedido = Column(ForeignKey('tbl_n_pedido.id_pedido'), nullable=False)
    descuento = Column(Numeric(10, 2))
    cantidad_facturada = Column(Integer)
    bonificacion = Column(Integer)
    cantidad_solicitada = Column(Integer)
    cantidad_entregada = Column(Integer)
    precio_real = Column(Numeric(10, 2))
    sub_total = Column(Numeric(10, 2))
    iva = Column(Numeric(10, 2))
    iva_retenido = Column(Numeric(10, 2))
    sub_total_mas_impuestos = Column(Numeric(10, 2))
    costo_unitario = Column(Numeric(10, 4))
    fecha_creacion = Column(Date)
    fecha_modificacion = Column(Date)

    tbl_n_estado_detalle_pedido = relationship('TblNEstadoDetallePedido')
    tbl_n_pedido = relationship('TblNPedido')


class TblNEmpleado(Base):
    __tablename__ = 'tbl_n_empleado'

    id_empleado = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_empleado_id_empleado_seq'::regclass)"))
    nombre_empleado = Column(String(30), nullable=False)
    activo = Column(Boolean, nullable=False)
    id_f_usuario = Column(ForeignKey('sch_seguridad.tbl_n_usuario.id_usuario'))

    tbl_n_usuario = relationship('TblNUsuario')


class TblNNegociacion(Base):
    __tablename__ = 'tbl_n_negociacion'

    id_negociacion = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_negociacion_id_negociacion_seq'::regclass)"))
    id_f_estado_negociacion = Column(ForeignKey('tbl_n_estado_negociacion.id_estado_negociacion'), nullable=False)
    id_f_tipo_negociacion = Column(ForeignKey('tbl_n_tipo_negociacion.id_tipo_negociacion'), nullable=False)
    id_f_producto = Column(ForeignKey('tbl_n_producto.id_producto'), nullable=False)
    id_f_alianza = Column(ForeignKey('tbl_n_alianza.id_alianza'), nullable=False)
    precio_compra_sin_iva = Column(Numeric(10, 4))
    precio_publico = Column(Numeric(10, 4))
    descuento = Column(Numeric(10, 2))
    precio_descuento = Column(Numeric(10, 4))
    precio_efectivo = Column(Numeric(10, 4))
    compra_promocion = Column(Integer)
    bonificacion_promocion = Column(Integer)
    minimo_promocion = Column(Integer)
    maximo_promocion = Column(Integer)
    descuento_aplicado_clinica = Column(Numeric(10, 2))
    precio_compra_con_iva = Column(Numeric(10, 4))
    precio_real_sin_iva = Column(Numeric(10, 4))
    precio_real_con_iva = Column(Numeric(10, 4))
    costo_unitario = Column(Numeric(10, 4))
    margen_contribucion = Column(Numeric(10, 4))
    retorno_inversion = Column(Numeric(10, 4))
    descuento_real_segun_compra = Column(Numeric(10, 4))
    descuento_real_segun_publico = Column(Numeric(10, 4))
    descuento_roi_50 = Column(Numeric(10, 2))
    descuento_roi_60 = Column(Numeric(10, 2))
    descuento_roi_70 = Column(Numeric(10, 2))
    descuento_roi_80 = Column(Numeric(10, 2))
    descuento_roi_90 = Column(Numeric(10, 2))
    descuento_roi_100 = Column(Numeric(10, 2))
    descuento_roi_110 = Column(Numeric(10, 2))
    descuento_roi_120 = Column(Numeric(10, 2))
    descuento_roi_130 = Column(Numeric(10, 2))
    cobra_iva = Column(Boolean, nullable=False)
    cobra_iva_retenido = Column(Boolean, nullable=False)
    fecha_creacion = Column(Date, nullable=False)
    fecha_modificacion = Column(Date, nullable=False)

    tbl_n_alianza = relationship('TblNAlianza')
    tbl_n_estado_negociacion = relationship('TblNEstadoNegociacion')
    tbl_n_producto = relationship('TblNProducto')
    tbl_n_tipo_negociacion = relationship('TblNTipoNegociacion')


class TblNTipoFormaPago(Base):
    __tablename__ = 'tbl_n_tipo_forma_pago'

    id_tipo_forma_pago = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_tipo_forma_pago_id_tipo_forma_pago_seq'::regclass)"))
    id_f_forma_pago = Column(ForeignKey('tbl_n_forma_pago.id_forma_pago'), nullable=False)
    id_f_tipo_pago = Column(ForeignKey('tbl_n_tipo_pago.id_tipo_pago'), nullable=False)
    id_f_entidad_bancaria = Column(ForeignKey('tbl_n_entidad_bancaria.id_entidad_bancaria'), nullable=False)

    tbl_n_entidad_bancaria = relationship('TblNEntidadBancaria')
    tbl_n_forma_pago = relationship('TblNFormaPago')
    tbl_n_tipo_pago = relationship('TblNTipoPago')


class TblNDocumentoConsumo(Base):
    __tablename__ = 'tbl_n_documento_consumo'

    id_documento_consumo = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_documento_consumo_id_documento_consumo_seq'::regclass)"))
    id_f_estado_documento_consumo = Column(ForeignKey('tbl_n_estado_documento_consumo.id_estado_documento_consumo'), nullable=False)
    id_f_empleado = Column(ForeignKey('tbl_n_empleado.id_empleado'), nullable=False)
    numero_documento = Column(String(60))
    fecha_creada = Column(Date)
    hora_creada = Column(Time)
    fecha_pagada = Column(Date)
    hora_pagada = Column(Time)
    deducible = Column(Numeric(10, 2))
    sub_total_sin_iva = Column(Numeric(10, 2))
    iva = Column(Numeric(10, 2))
    pago_por = Column(Numeric(10, 2))
    comision_entidad = Column(Numeric(10, 2))
    retencion_gubernamental = Column(Numeric(10, 2))
    extra_tc = Column(Numeric(10, 2))
    recargo = Column(Numeric(10, 2))
    total_sin_descuento = Column(Numeric(10, 2))
    total = Column(Numeric(10, 2))
    total_deber = Column(Numeric(10, 2))
    efectivo = Column(Numeric(10, 2))
    vuelto = Column(Numeric(10, 2))
    numero_reserva = Column(String(15))
    fecha_limite = Column(Date)
    codigo_pagalink = Column(String(100))
    numero_transaccion_transferencia = Column(String(100))
    fecha_transaccion = Column(Date)
    fecha_transaccion_pagalink = Column(Date)

    tbl_n_empleado = relationship('TblNEmpleado')
    tbl_n_estado_documento_consumo = relationship('TblNEstadoDocumentoConsumo')


class TblNInventario(Base):
    __tablename__ = 'tbl_n_inventario'

    id_inventario = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_inventario_id_inventario_seq'::regclass)"))
    id_f_tipo_inventario = Column(ForeignKey('tbl_n_tipo_inventario.id_tipo_inventario'), nullable=False)
    id_f_representante_proveedor = Column(ForeignKey('tbl_n_representante_proveedor.id_representante_proveedor'), nullable=False)
    id_f_negociacion = Column(ForeignKey('tbl_n_negociacion.id_negociacion'), nullable=False)

    tbl_n_negociacion = relationship('TblNNegociacion')
    tbl_n_representante_proveedor = relationship('TblNRepresentanteProveedor')
    tbl_n_tipo_inventario = relationship('TblNTipoInventario')


class TblNLote(Base):
    __tablename__ = 'tbl_n_lote'

    id_lote = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_lote_id_lote_seq'::regclass)"))
    cantidad_ingresada = Column(Integer, nullable=False)
    cantidad_disponible = Column(Integer, nullable=False)
    cantidad_reservada = Column(Integer, nullable=False)
    id_f_detalle_pedido = Column(ForeignKey('tbl_n_detalle_pedido.id_detalle_pedido'), nullable=False)
    id_f_lote_predecesor = Column(ForeignKey('tbl_n_lote.id_lote'))
    id_f_estado_lote = Column(ForeignKey('tbl_n_estado_lote.id_estado_lote'), nullable=False)
    fecha_creacion = Column(Date)
    fecha_modificacion = Column(Date)

    tbl_n_detalle_pedido = relationship('TblNDetallePedido')
    tbl_n_estado_lote = relationship('TblNEstadoLote')
    parent = relationship('TblNLote', remote_side=[id_lote])


class TblNSucursalNegociacion(Base):
    __tablename__ = 'tbl_n_sucursal_negociacion'

    id_sucursal_negociacion = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_sucursal_negociacion_id_sucursal_negociacion_seq'::regclass)"))
    id_f_sucursal = Column(ForeignKey('tbl_n_sucursal.id_sucursal'), nullable=False)
    id_f_negociacion = Column(ForeignKey('tbl_n_negociacion.id_negociacion'), nullable=False)
    activo = Column(Boolean, nullable=False, server_default=text("true"))

    tbl_n_negociacion = relationship('TblNNegociacion')
    tbl_n_sucursal = relationship('TblNSucursal')


class TblNBodegaInventario(Base):
    __tablename__ = 'tbl_n_bodega_inventario'

    id_bodega_inventario = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_bodega_inventario_id_bodega_inventario_seq'::regclass)"))
    id_f_inventario = Column(ForeignKey('tbl_n_inventario.id_inventario'), nullable=False)
    id_f_bodega = Column(ForeignKey('tbl_n_bodega.id_bodega'), nullable=False)
    fecha_creacion = Column(Date, nullable=False)
    fecha_modificacion = Column(Date, nullable=False)
    activo = Column(Boolean, nullable=False)

    tbl_n_bodega = relationship('TblNBodega')
    tbl_n_inventario = relationship('TblNInventario')


class TblNDetalleAsignacionPedido(Base):
    __tablename__ = 'tbl_n_detalle_asignacion_pedido'

    id_detalle_asignacion_pedido = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_detalle_asignacion_pedid_id_detalle_asignacion_pedido_seq'::regclass)"))
    id_f_asignacion_pedido = Column(ForeignKey('tbl_n_asignacion_pedido.id_asignacion_pedido'), nullable=False)
    id_f_estado_detalle_asignacion_pedido = Column(ForeignKey('tbl_n_estado_detalle_asignacion_pedido.id_estado_detalle_asignacion_pedido'), nullable=False)
    id_f_representante_proveedor = Column(ForeignKey('tbl_n_representante_proveedor.id_representante_proveedor'), nullable=False)
    id_f_sucursal_negociacion = Column(ForeignKey('tbl_n_sucursal_negociacion.id_sucursal_negociacion'), nullable=False)
    cantidad = Column(Integer)
    fecha_creacion = Column(Date)
    fecha_modificacion = Column(Date)

    tbl_n_asignacion_pedido = relationship('TblNAsignacionPedido')
    tbl_n_estado_detalle_asignacion_pedido = relationship('TblNEstadoDetalleAsignacionPedido')
    tbl_n_representante_proveedor = relationship('TblNRepresentanteProveedor')
    tbl_n_sucursal_negociacion = relationship('TblNSucursalNegociacion')


class TblNDetalleConsumo(Base):
    __tablename__ = 'tbl_n_detalle_consumo'

    id_detalle_consumo = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_detalle_consumo_id_detalle_consumo_seq'::regclass)"))
    id_f_documento_consumo = Column(ForeignKey('tbl_n_documento_consumo.id_documento_consumo'), nullable=False)
    id_f_estado_detalle_consumo = Column(ForeignKey('tbl_n_estado_detalle_consumo.id_estado_detalle_consumo'), nullable=False)
    id_f_tipo_forma_pago = Column(ForeignKey('tbl_n_tipo_forma_pago.id_tipo_forma_pago'), nullable=False)
    cantidad = Column(Integer)
    precio = Column(Numeric(10, 2))
    descuento = Column(Numeric(10, 4))
    precio_sin_descuento = Column(Numeric(10, 2))
    deducible = Column(Numeric(10, 2))
    deducible_no_aproximado = Column(Numeric(16, 8))
    total_deducible = Column(Numeric(10, 2))
    total_debe = Column(Numeric(10, 2))
    sub_total_sin_iva = Column(Numeric(10, 2))
    iva = Column(Numeric(10, 2))
    pago_por = Column(Numeric(10, 2))
    comision_entidad = Column(Numeric(10, 2))
    retencion_gubernamental = Column(Numeric(10, 2))
    extra_tc = Column(Numeric(10, 2))
    recargo = Column(Numeric(10, 2))
    total_sin_descuento = Column(Numeric(10, 2))
    total = Column(Numeric(12, 6))
    fecha_creacion = Column(Date, nullable=False)
    fecha_modificacion = Column(Date, nullable=False)

    tbl_n_documento_consumo = relationship('TblNDocumentoConsumo')
    tbl_n_estado_detalle_consumo = relationship('TblNEstadoDetalleConsumo')
    tbl_n_tipo_forma_pago = relationship('TblNTipoFormaPago')


class TblNBodegaLote(Base):
    __tablename__ = 'tbl_n_bodega_lote'

    id_bodega_lote = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_bodega_lote_id_bodega_lote_seq'::regclass)"))
    id_f_lote = Column(ForeignKey('tbl_n_lote.id_lote'), nullable=False)
    id_f_bodega_inventario = Column(ForeignKey('tbl_n_bodega_inventario.id_bodega_inventario'), nullable=False)
    fecha_creacion = Column(Date, nullable=False)
    fecha_modificacion = Column(Date, nullable=False)

    tbl_n_bodega_inventario = relationship('TblNBodegaInventario')
    tbl_n_lote = relationship('TblNLote')


class TblNDetalleConsumoLote(Base):
    __tablename__ = 'tbl_n_detalle_consumo_lote'

    id_detalle_consumo_lote = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_detalle_consumo_lote_id_detalle_consumo_lote_seq'::regclass)"))
    cantidad = Column(Integer, nullable=False)
    id_f_detalle_consumo = Column(ForeignKey('tbl_n_detalle_consumo.id_detalle_consumo'), nullable=False)
    id_f_lote = Column(ForeignKey('tbl_n_lote.id_lote'), nullable=False)
    fecha_creacion = Column(Date)

    tbl_n_detalle_consumo = relationship('TblNDetalleConsumo')
    tbl_n_lote = relationship('TblNLote')


class TblNDetalleDevolucion(Base):
    __tablename__ = 'tbl_n_detalle_devolucion'

    id_detalle_devolucion = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_detalle_devolucion_id_detalle_devolucion_seq'::regclass)"))
    id_f_sucursal_negociacion = Column(ForeignKey('tbl_n_sucursal_negociacion.id_sucursal_negociacion'), nullable=False)
    id_f_solicitud_devolucion = Column(ForeignKey('tbl_n_solicitud_devolucion.id_solicitud_devolucion'), nullable=False)
    id_f_estado_detalle_devolucion = Column(ForeignKey('tbl_n_estado_detalle_devolucion.id_estado_detalle_devolucion'), nullable=False)
    id_f_lote = Column(ForeignKey('tbl_n_lote.id_lote'), nullable=False)
    id_f_detalle_consumo = Column(ForeignKey('tbl_n_detalle_consumo.id_detalle_consumo'), nullable=False)
    cantidad_devolucion = Column(Integer)
    fecha_creacion = Column(Date)
    fecha_modificacion = Column(Date)
    cantidad_devuelta = Column(Integer)

    tbl_n_detalle_consumo = relationship('TblNDetalleConsumo')
    tbl_n_estado_detalle_devolucion = relationship('TblNEstadoDetalleDevolucion')
    tbl_n_lote = relationship('TblNLote')
    tbl_n_solicitud_devolucion = relationship('TblNSolicitudDevolucion')
    tbl_n_sucursal_negociacion = relationship('TblNSucursalNegociacion')


class TblNAsignacionPedidoBodega(Base):
    __tablename__ = 'tbl_n_asignacion_pedido_bodega'

    id_asignacion_pedido_bodega = Column(Integer, primary_key=True, server_default=text("nextval('tbl_n_asignacion_pedido_bodega_id_asignacion_pedido_bodega_seq'::regclass)"))
    id_f_bodega_lote = Column(ForeignKey('tbl_n_bodega_lote.id_bodega_lote'))
    id_f_detalle_pedido = Column(ForeignKey('tbl_n_detalle_pedido.id_detalle_pedido'))
    fecha_creacion = Column(Date)
    fecha_modificacion = Column(Date)
    cantidad = Column(Integer)
    id_f_asignacion_pedido = Column(Integer, nullable=False)
    id_f_detalle_asignacion_pedido = Column(ForeignKey('tbl_n_detalle_asignacion_pedido.id_detalle_asignacion_pedido'), nullable=False)

    tbl_n_bodega_lote = relationship('TblNBodegaLote')
    tbl_n_detalle_asignacion_pedido = relationship('TblNDetalleAsignacionPedido')
    tbl_n_detalle_pedido = relationship('TblNDetallePedido')
