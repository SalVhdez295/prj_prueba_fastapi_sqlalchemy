1. instalar python,pip
2. instalar anaconda (agregar al path)
3. crear un venv en conda y activarlo.
3.1 conda create -n nombreenv python=3
3.2 conda activate nombreenv
3.3 para desactivar conda deactivate nombreenv
4. instalar con pip sqlacodegen (pip install sqlacodegen)
5. instalar con pip uvicorn (pip install uvicorn)
6  ejecutar archivo build.sh

*es necesario haber ejecutado el script de la bd de drogueria, y correrlo en 
una base limpia de nombre bd_drogueria
