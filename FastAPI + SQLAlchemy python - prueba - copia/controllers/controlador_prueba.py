from sqlite3 import connect
from models.db import TblNUsuario
from config.db_connection import connection
from typing import List
def primer_servicio():
    return "primer_servicio_respuesta"


def servicio_prueba_usuarios():
    lista_usuarios = TblNUsuario.select().all()

    return lista_usuarios

def servicio_prueba_usuarios_2():
    lista_usuarios = connection.query(TblNUsuario).all()
    return lista_usuarios

def servicio_prueba_usuarios_3():
    lista_usuarios: List(TblNUsuario) = connection.query(TblNUsuario).all()
    return lista_usuarios
